#!/bin/bash

pids=()

function run {
    local cmd="$*"
    local result

    $cmd
    result=$?

    if [ $result != 0 ]; then
        echo "Error when running command: '$cmd'"
        exit $result
    fi
}

function spawn {
    local cmd="$*"
    local result
    local pid

    $cmd > /dev/null &
    result=$?
    pid=$!
    pids=("${pids[@]}" "$pid")

    if [ $result != 0 ]; then
        echo "Error when running command: '$cmd'"
        exit $result
    fi
}

function bailout() {
    local message="$1"

    if [ -z "$message" ]; then
        message="Quitting."
    fi

    echo "$message"
    exit 1
}

function rkill() {
    local pid="$1"

    # Unlink from current process
    run disown "$pid"

    # Stop
    run kill "$pid"

    # Kill child processes
    for cpid in $(ps -o pid,ppid -ax | awk "{ if ( \"$2\" == \"$pid\" ) { print \"$1\" }}"); do
        rkill "$cpid"
    done
}

function cleanup() {
    # Recursively kill all spawned processes
    for pid in "${pids[@]}"; do
        if [ ! -z "$pid" ]; then
            rkill "$pid"
        fi
    done

    echo "All servers stopped."
}

# Catch errors
set -e

# Clean up on exit
trap cleanup 0

script_dir=$(cd $(dirname "$0") && pwd)
root_dir=$(dirname "$script_dir")
app1_dir="$root_dir/acme-app1"
app2_dir="$root_dir/acme-app2"
app3_dir="$root_dir/acme-app3"

run cd "$app1_dir"
spawn php -S 0.0.0.0:8040 -t web

run cd "$app2_dir"
spawn php -S 0.0.0.0:8050 -t web

run cd "$app3_dir"
spawn php -S 0.0.0.0:8060 -t web

echo "Servers running:"
echo "acme-app1: http://localhost:8040/app_dev.php"
echo "acme-app2: http://localhost:8050/app_dev.php"
echo "acme-app3: http://localhost:8060/app_dev.php"
echo "Press ENTER to quit."

read
