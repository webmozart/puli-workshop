The Puli Workshop Repository
============================

Getting Started
---------------

Install the workshop repository on your system:

```
$ git clone https://bitbucket.org/webmozart/puli-workshop.git
```

Once you checked out the repository, run:

```
$ bin/install
```

If you are running on a Unix system, add the following lines to `~/.bashrc`:

```
alias puli='set -f;puli';puli(){ command puli "$@";set +f;}
```

Reload your shell:

```
$ source ~/.bashrc
```

Finally, start the workshop servers in a separate Window:

```
$ bin/run-server
```

You should be able to open http://localhost:8040/app_dev.php in your browser. 
The expected result is a `Twig_Loader_Error`.
